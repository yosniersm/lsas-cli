#!/bin/bash

file1="/Users/ysamonma/OSP/projects/LSAS/lsas-cli/csv/input/product/lsas-quality-ldh-duplicated-products.csv"
file2="/Users/ysamonma/OSP/projects/LSAS/lsas-cli/csv/input/product/mola-quality-products.csv"
output_file="./output.csv"
column_names=("productnumber" "size")
additional_column="erpproductid"

# Create a string of column indices to match on
column_indices=""
for column_name in "${column_names[@]}"; do
    column_index=$(head -1 "$file1" | tr ',' '\n' | grep -n "^$column_name$" | cut -d: -f1)
    column_indices="$column_indices $column_index"
done

# Get the index of the additional column
additional_column_index=$(head -1 "$file2" | tr ',' '\n' | grep -n "^$additional_column$" | cut -d: -f1)

# Generate the output file
awk -F, -v file2="$file2" -v column_indices="$column_indices" -v additional_column_index="$additional_column_index" '
    BEGIN {split(column_indices, indices, " ")}
    NR==1 {print $0","additional_column; next}
    NR==FNR {a[$0]=$additional_column_index; next}
    {
        match=1
        for (i in indices) {
            split($0, arr1, ",")
            split(a[$0], arr2, ",")
            if (arr1[indices[i]] != arr2[indices[i]]) {
                match=0
                break
            }
        }
        if (match) {
            print $0","a[$additional_column_index]
        }
    }
' "$file1" "$file2" > "$output_file"