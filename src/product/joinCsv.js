const csv = require('csv-parser');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const path = require('path');

const readCsv = (filename) => {
  const data = [];
  return new Promise((resolve, reject) => {
    fs.createReadStream(filename)
      .pipe(csv())
      .on('data', (row) => data.push(row))
      .on('end', () => resolve(data))
      .on('error', reject);
  });
};

const writeCsv = (filename, data) => {
  const csvWriter = createCsvWriter({
    path: filename,
    header: Object.keys(data[0]).map((key) => ({ id: key, title: key })),
  });
  return csvWriter.writeRecords(data);
};

const generateCombinedCsv = async (directory, output_file) => {
  const files = fs.readdirSync(directory);
  const csvFiles = files.filter((file) => path.extname(file) === '.csv');

  let allData = [];
  for (const file of csvFiles) {
    const data = await readCsv(path.join(directory, file));
    allData = allData.concat(data);
  }

  if (allData.length > 0) {
    await writeCsv(output_file, allData);
  }
};

generateCombinedCsv('./csv_directory', 'output.csv');