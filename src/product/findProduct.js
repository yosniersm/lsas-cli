const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { Client } = require('pg');
const dbConfig = require('../config/env')();

// const csvData = [];

const basePath = process.env.PWD;
const csvPath = `${basePath}/csv`;
const csvInputPath = `${csvPath}/input/product/live-products`;
const csvOutputPath = `${csvPath}/output/product/live-products`;

const csvInputFile = `${csvInputPath}/20240104_bquxjob_6603d2db_18cd4bd299e.csv`;


const exportToCsv = async (data = [], fileName) => {
  const toExport = data.map(row => ({
    ...row,
    creationtime: row.creationtime 
      ? new Date(row.creationtime).toISOString().slice(0, 19)
      : undefined,
    updatetime: row.updatetime 
      ? new Date(row.updatetime).toISOString().slice(0, 19)
      : undefined,
  }));
  const currentDate = new Date().toISOString().slice(0, 10).replaceAll('-', '');
  const selectedFileName = `${currentDate}_${fileName}.csv`;

  console.log(`Exporting ${data.length} rows to ${selectedFileName}...`);
  const csvWriterForExists = createCsvWriter({
    path: `${csvOutputPath}/${selectedFileName}`,  // path to csv file
    header: [ ...Object.keys(data[0]).map(key => ({id: key, title: key})) ]
  });

  await csvWriterForExists.writeRecords(toExport);

}

const processFile = async (pathFile, csvData = []) => {
  fs.createReadStream(pathFile)
      .pipe(csv())
      .on('data', (row) => {
        csvData.push(row);
      })
      .on('end', async () => {
        const client = new Client({
          host: dbConfig.host,
          port: dbConfig.port,
          user: dbConfig.user,
          password: dbConfig.password,
          database: dbConfig.database
        });

        try {
          await client.connect();

          // Use 'erp_product_id' for matching
          // ==================================

          const erpProductIds = [];
          const iocks = [];
          csvData.forEach(row => {
            erpProductIds.push(`'${row.erpProductId}'`)
            iocks.push(`'${row.item_option_comm_key}'`)
          });
          const where = `WHERE erp_id = 2`;
          
          const query = `
            SELECT id, logisticsproductid, erp_id, supplier_id, erpproductid, productnumber, item_option_comm_key, size, active, season, creationtime, updatetime
            FROM product
            ${where}`;

          console.log(query);

          const res = await client.query(query)
          console.log(`${res.rows.length} products found`);
        
          if (res.rows.length) {
            // Export exists products to CSV
            // ==============================
            await exportToCsv(res.rows, '20240110-ldh-live-products')
              .catch(err => {
                console.log(err);
              })
            
            
            // Export difference to CSV
            // =========================
            // console.log(`Searching difference with lsas products...`)
            // const difference = csvData.filter(row => !res.rows.find(product => 
            //   product.item_option_comm_key === row.item_option_comm_key && 
            //   product.erpproductid === row.erpProductId
            // ));

            // console.log(`${difference.length} products not found in database`);
            // if (difference.length) {
            //   await exportToCsv(difference, 'missing-in-lsas_products');
            // }

            console.log('The CSVs files was written successfully');

            //Update products
            // ===============

            // console.log('Updating products...');
            // const updateQuery = `
            //   UPDATE lsas_quality.product 
            //   SET active = false 
            //   ${where}`;

            // await client.query(updateQuery);
            // console.log('Updated successfully');


            //Delete products
            // ===============
          // console.log('Deleting products...');

            // const selectedIds = res.rows.map(row => row.id);
            // const supplierProductQuery = `
            //   SELECT id FROM lsas_quality.supplier_product where product_entity_id IN (${selectedIds.join(',')})
            // `

            // const supplierProductResp = await client.query(supplierProductQuery);
            // const supplierProductIds = supplierProductResp.rows.map(row => row.id);
            

            // // Delete supplier_product and supplier_product_ean
            // if (supplierProductIds.length) {
            //   const deleteSupplierProductEanQuery = `
            //     DELETE FROM lsas_quality.supplier_product_ean
            //     WHERE supplier_product_entity_id IN (${supplierProductIds.join(',')})
            //     RETURNING *
            //   `
            //   console.log(deleteSupplierProductEanQuery);
            //   const deletingSupplierProductEanResp = await client.query(deleteSupplierProductEanQuery);
            //   console.log(deletingSupplierProductEanResp.rows);
            //   if (deletingSupplierProductEanResp.rows.length){
            //     await exportToCsv(deletingSupplierProductEanResp.rows, 'deleted_supplier_product_ean')
            //   }
            //   console.log(`Deleted supplier_product_ean ${deletingSupplierProductEanResp.rows}`);

            //   // Delete supplier_product
            //   const deleteSupplierProductQuery = `
            //     DELETE FROM lsas_quality.supplier_product
            //     WHERE id IN (${supplierProductIds.join(',')})
            //     RETURNING *
            //   `
            //   console.log(deleteSupplierProductQuery);
            //   const deletingSupplierProductResp = await client.query(deleteSupplierProductQuery);
            //   if (deletingSupplierProductResp.rows.length){
            //     await exportToCsv(deletingSupplierProductResp.rows, 'deleted_supplier_product')
            //   }

            //   console.log(`Deleted supplier_product ${deletingSupplierProductResp.rows}`);
            // }

            // // Delete product_package
            // const packageQuery = `
            //   SELECT id FROM lsas_quality.product_package where product_id IN (${selectedIds.join(',')})
            // ` 
            // const packageResp = await client.query(packageQuery);
            // const packageIds = packageResp.rows.map(row => row.id);
            // console.log(packageResp.rows);

            // if(packageIds.length) {
            // //    Delete product
            //   const productPackageDeleteQuery = `
            //     DELETE FROM lsas_quality.product_package
            //     WHERE id IN (${packageIds.join(',')})
            //     RETURNING *
            //   `;

            //    const productPackageDeletingResp = await client.query(productPackageDeleteQuery);
            //     console.log(`Deleted product_package ${productPackageDeletingResp.rows}`);
            //     if (productPackageDeletingResp.rows.length){
            //       await exportToCsv(productPackageDeletingResp.rows, 'deleted_product_package')
            //      }
            // }

            // // Delete product_client_systems
            // // const clientSystemQuery = `
            // //   SELECT * FROM lsas_quality.product_client_systems where product_id IN (${selectedIds.join(',')})
            // // ` 
            // // const productClientSystemResp = await client.query(clientSystemQuery);
            // // const clientSystemIds = productClientSystemResp.rows.map(row => row.client_id);
            // // console.log(productClientSystemResp.rows);

            // // if(clientSystemIds.length) {

              
            // //   // export 
            // //   await exportToCsv(productClientSystemResp.rows, 'deleted_product_client_systems')

            // //   // Delete product client systems
            // //     const productClientSystemsDeleteQuery = `
            // //       DELETE FROM lsas_quality.product_client_systems
            // //       WHERE (client_id, product_id) IN (
            // //         select unnest(array [${clientSystemIds.join(',')}]), unnest(array [${selectedIds.join(',')}])
            // //       )
            // //     `;
      
            // //   await client.query(productClientSystemsDeleteQuery);
              
            // // }

            // // Delete product

            // await exportToCsv(res.rows, 'deleted_product')
            // const deleteQuery = `
            //   DELETE FROM lsas_quality.product
            //   ${where}
            //   RETURNING *
            // `;

            // const deletingProductResp = await client.query(deleteQuery);
            // console.log(`Deleted products ${deletingProductResp.rows}`);

            // console.log('Deleting successfully');


            // // Send Products to Kafka through lsas-service
            // // ============================================

            // const insertProductExportQuery = `
            //   INSERT INTO product_export
            //   select nextval('seq_artikelgroesse_export'), id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1
            //   from lsas_quality.product
            //   ${where}`;

            // console.log(`Sending ${res.rows.length} products to Kafka...`);

            // await client.query(insertProductExportQuery);

            // console.log('Sent product to Kafka successfully');
          }

        } catch (err) {
          console.error(err);
        } finally {
          await client.end();
        }
      });
}

console.log('Scanning CSV files...');
fs.readdir(csvInputPath, async (err, files) => {
  if (err) {
    return console.error('Unable to scan directory: ' + err);
  }

  // filter out the CSV files
  const csvFiles = files.filter(file => path.extname(file).toLowerCase() === '.csv');
  
  const csvName = csvFiles[12];
  console.log(`Processing file ${csvName}...`);
  const pathFile = `${csvInputPath}/${csvName}`;
  await processFile(pathFile).catch(err => console.error(err));

  // csvFiles.forEach(async file => {
  //   console.log(`Processing file ${file}...`);
  //   const csvData = [];
  //   const pathFile = `${csvInputPath}/${file}`;
  //   await processFile(pathFile);
    
  // });
});