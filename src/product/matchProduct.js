const csv = require('csv-parser');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const basePath = process.env.PWD;
const csvPath = `${basePath}/csv`;
const csvInputPath = `${csvPath}/input/product`;
const csvOutputPath = `${csvPath}/output/product`;

const readCsv = (filename) => {
  const data = [];
  return new Promise((resolve, reject) => {
    fs.createReadStream(filename)
      .pipe(csv())
      .on('data', (row) => data.push(row))
      .on('end', () => resolve(data))
      .on('error', reject);
  });
};

const writeCsv = (filename, data) => {
  const csvWriter = createCsvWriter({
    path: filename,
    header: Object.keys(data[0]).map((key) => ({ id: key, title: key })),
  });
  return csvWriter.writeRecords(data);
};


const generateMatchingCsv = async (file1, file2, output_file, column_names, additional_column) => {
    const data1 = await readCsv(file1);
    const data2 = await readCsv(file2);
  
    const matching_data = data1.map((row1) => {
      const matchingRow2 = data2.find((row2) => 
        column_names.every(column_name => row1[column_name] === row2[column_name])
      );
      if (matchingRow2) {
        return {...row1, [additional_column]: matchingRow2[additional_column]};
      }
      return null;
    }).filter(Boolean);
  
    if (matching_data.length > 0) {
      await writeCsv(output_file, matching_data);
    }
  };
  
  generateMatchingCsv(
    `${csvInputPath}/lsas-quality-ldh-duplicated-products.csv`, 
    `${csvInputPath}/mola-quality-products.csv`, 
    `${csvOutputPath}/output.csv`, 
    ['productnumber', 'size'], 
    'erpproductid'
  );

// const generateMatchingCsv = async (file1, file2, output_file, column_names) => {
//     const data1 = await readCsv(file1);
//     const data2 = await readCsv(file2);
  
//     const matching_data = data1.filter((row1) =>
//       data2.some((row2) => 
//         column_names.every(column_name => row1[column_name] === row2[column_name])
//       )
//     );
  
//     if (matching_data.length > 0) {
//       await writeCsv(output_file, matching_data);
//     }
// };

// generateMatchingCsv('file1.csv', 'file2.csv', 'output.csv', ['column_name1', 'column_name2']);