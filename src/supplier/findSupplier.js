const fs = require('fs');
const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { Client } = require('pg');
const dbConfig = require('../config/env')();

const csvData = [];

const basePath = process.env.PWD;
const csvPath = `${basePath}/csv`;
const csvInputPath = `${csvPath}/input/product`;
const csvOutputPath = `${csvPath}/output/product`;

const csvInputFile = `${csvInputPath}/20231220_inactive_bonprix_products-lsas.csv`;

console.log(`Processing file ${csvInputFile}...`);
fs.createReadStream(csvInputFile)
  .pipe(csv())
  .on('data', (row) => {
    csvData.push(row);
  })
  .on('end', async () => {
    const client = new Client({
      host: dbConfig.host,
      port: dbConfig.port,
      user: dbConfig.user,
      password: dbConfig.password,
      database: dbConfig.database
    });

    try {
      await client.connect();

      // Use 'erp_product_id' for matching
      const ids = csvData.map(row => `'${row.erp_product_id}'`);
      const where = `WHERE  id IN (607616)`;
      const query = `
        SELECT id, logisticsproductid, erp_id, supplier_id, erpproductid, productnumber, size, active, season, creationtime, updatetime
        FROM lsas_quality.product 
        ${where}`;

      const res = await client.query(query)
      console.log(res.rows);
      console.log(`${res.rows.length} products found`);

     
      if (res.rows.length > 0) {
         // Export exists products to CSV
         // ==============================
        const toExport = res.rows.map(row => ({
            ...row,
            creationtime: new Date(row.creationtime).toISOString().slice(0, 19),
            updatetime: new Date(row.updatetime).toISOString().slice(0, 19),
        }));
        const currentDate = new Date().toISOString().slice(0, 10).replaceAll('-', '');
        const selectedFileName = `${currentDate}_selected_products.csv`;

        const csvWriterForExists = createCsvWriter({
          path: `${csvOutputPath}/${selectedFileName}`,  // path to csv file
          header: [
            {id: 'id', title: 'id'},  // column names to be exported
            {id: 'logisticsproductid', title: 'logistics_product_id'},
            {id: 'erp_id', title: 'erp_id'},
            {id: 'supplier_id', title: 'supplier_id'},
            {id: 'erpproductid', title: 'erp_product_id'},
            {id: 'productnumber', title: 'product_number'},
            {id: 'size', title: 'size'},
            {id: 'active', title: 'active'},
            {id: 'season', title: 'season'},
            {id: 'creationtime', title: 'creationtime'},
            {id: 'updatetime', title: 'updatetime' },
          ]
        });
        await csvWriterForExists.writeRecords(toExport);


        // Export difference to CSV
        // =========================
        const difference = csvData.filter(row => !res.rows.find(product => product.erpproductid === row.erp_product_id));
        console.log(`${difference.length} products not found in database`);
        const differenceFileName = `${currentDate}_missing-in-lsas_products.csv`;

        const csvWriterForDifference = createCsvWriter({
          path: `${csvOutputPath}/${differenceFileName}`,  // path to csv file
          header: [
            {id: 'product_number', title: 'product_number'},  // column names to be exported
            {id: 'size', title: 'size'},
            {id: 'season', title: 'season'},
            {id: 'erp_product_id', title: 'erp_product_id'},
          ]
        });
        await csvWriterForDifference.writeRecords(difference);

        console.log('The CSVs files was written successfully');

        //Update products
        // ===============

        const updateQuery = `
          UPDATE lsas_quality.product 
          SET active = false 
          ${where}`;

        await client.query(updateQuery);
        console.log('Updated successfully');


        // Send Products to Kafka through lsas-service
        // ============================================

        const insertProductExportQuery = `
          INSERT INTO product_export
          select nextval('seq_artikelgroesse_export'), id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1
          from lsas_quality.product
          ${where}`;

        console.log(`Sending ${res.rows.length} products to Kafka...`);

        await client.query(insertProductExportQuery);

        console.log('Sent product to Kafka successfully');
      }

    } catch (err) {
      console.error(err);
    } finally {
      await client.end();
    }
  });

