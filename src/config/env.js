require('dotenv').config();

const checkEnv = (value) => {
    if (!value){
        console.warn(`Env variable ${value} is not set. Please setup the database config`);
        process.exit(1);
    }
    return value;
};

module.exports = () => {
    const {
        DB_HOST,
        DB_PORT,
        DB_USER,
        DB_PASSWORD,
        DB_NAME,
    } = process.env;
    
    const config = {
        host: checkEnv(DB_HOST),
        port: checkEnv(DB_PORT),
        user: checkEnv(DB_USER),
        password: checkEnv(DB_PASSWORD),
        database: checkEnv(DB_NAME),
    };

    return config;

}






